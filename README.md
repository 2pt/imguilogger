## IMGUILogger
Unity immediate mode GUI visual logger and debugging system.

### Usage
* (Optional) Add the script to an object in the scene
* Call `IMGUILogger.Log(key: string, value: object)` from any script
* Log will display as '{key}:{value.ToString()}' in game view
* Add entry to `filter` in inspector to filter the logs
* Call `IMGUILogger.LogPersistent()` to persist log rendering on to the screen
* Call `IMGUILogger.RemovePersistentLog(key: string)` to remove a given persistent log from the the screen