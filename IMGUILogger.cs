﻿/*
 * IMGUILogger
 * Displays logs to Unity game view via legacy IMGUI system.
 *
 * Alexander Perrin, 2017
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IMGUILogger : MonoBehaviour
{
    private static Dictionary<string, LogData> _logs = new Dictionary<string, LogData>();
    private int _filteredLogs;
    private static Stack<string> _removals = new Stack<string>();
    private static IMGUILogger __instance;
    public static IMGUILogger Instance
    {
        get
        {
            return CheckInstance();
        }
    }

    private class LogData
    {
        public Color color { get; private set; }
        public object value { get; private set; }
        public bool persist { get; private set; }
        public int fontSize { get; private set; }

        private string key;

        public LogData(string key, object value, Color color, bool persist, int fontSize)
        {
            this.key = key;
            this.value = value;
            this.color = color;
            this.fontSize = fontSize;
        }

        public void SetValue(object value)
        {
            this.value = value;
        }
    }

    public Color defaultColor = Color.white;
    public Color defaultShadowColor = Color.black;
    public int defaultFontSize = 24;
    public Vector2 shadowDistance = new Vector2(0f, 0.05f);
    public string filter;
    public bool showLogs = true;

    /// <summary>
    /// Add a log to the logger.
    /// </summary>
    /// <param name="key">The identifier and display name of the log.</param>
    /// <param name="value">The value to log.</param>
    /// <param name="color">The override color to display the log with.</param>
    /// <param name="persist">Should the log persist between updates?</param>
    public static void Log(string key, object value, Color color, bool persist, int fontSize)
    {
        CheckInstance();
        if (_logs.ContainsKey(key))
        {
            _logs[key].SetValue(value);
        }
        else
        {
            _logs.Add(key, new LogData(key, value, color, persist, fontSize));
        }
    }

    public static void Log(string key, object value, Color color, bool persist)
    {
        Log(key, value, color, persist, Instance.defaultFontSize);
    }

    public static void Log(string key, object value)
    {
        Log(key, value, Instance.defaultColor, false);
    }

    public static void Log(string key, object value, Color color)
    {
        Log(key, value, color, false);
    }

    /// <summary>
    /// Add the log to the screen persistently.
    /// Log will only be updated when this is called again.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="color"></param>
    public static void LogPersistent(string key, object value, Color color)
    {
        Log(key, value, color, true);
    }

    public static void LogPersistent(string key, object value)
    {
        Log(key, value, Instance.defaultColor, true);
    }

    /// <summary>
    /// Remove a persistent log from the logger.
    /// </summary>
    /// <param name="key"></param>
    public static void RemovePersistentLog(string key)
    {
        if (_logs.ContainsKey(key))
        {
            _logs.Remove(key);
        }
    }

    /// <summary>
    /// Check for IMGUILogger intance and create if not found.
    /// </summary>
    private static IMGUILogger CheckInstance()
    {
        if (__instance == null)
        {
            __instance = FindObjectOfType<IMGUILogger>();
        }
        if (__instance == null)
        {
            __instance = new GameObject("IMGUILogger", typeof(IMGUILogger)).GetComponent<IMGUILogger>();
        }
        return __instance;
    }

    protected void OnGUI()
    {
        if (!showLogs)
        {
            return;
        }

        foreach (string key in _logs.Keys)
        {
            // Don't display any key values that aren't in the current filter string
            if (filter.Length > 0 && !key.ToLowerInvariant().Contains(filter.ToLowerInvariant()))
            {
                _filteredLogs++;
                continue;
            }

            // Draw each of the log values to screen
            LogData d = _logs[key];
            string text = string.Format("{0}: {1}", key, d.value.ToString());
            DrawShadowedText(text, d.color, d.fontSize);
        }

        if (_filteredLogs > 0)
        {
            // Display message for filtered logs
            DrawShadowedText(string.Format("({0} logs hidden with filter \"{1}\")", _filteredLogs, filter), defaultColor, defaultFontSize);
            _filteredLogs = 0;
        }
    }

    private void DrawShadowedText(string text, Color color, int fontSize)
    {
        GUI.skin.label.fontSize = fontSize;
        GUIContent content = new GUIContent(text);
        GUI.color = Instance.defaultShadowColor;
        GUILayout.Label(content);
        Rect r = GUILayoutUtility.GetLastRect();
        r.position = new Vector2(
            r.position.x - fontSize * shadowDistance.x,
            r.position.y - fontSize * shadowDistance.y
        );
        GUI.color = color;
        GUI.Label(r, content);
    }

    protected void Update()
    {
        // Remove any non-persistent logs that weren't updated in the last frame
        // Stored in removal stack to prevent dict sync issues
        while (_removals.Count > 0)
        {
            _logs.Remove(_removals.Pop());
        }
        foreach (string key in _logs.Keys)
        {
            if (!_logs[key].persist)
            {
                _removals.Push(key);
            }
        }
    }
}
